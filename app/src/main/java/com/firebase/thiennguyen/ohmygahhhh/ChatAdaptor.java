package com.firebase.thiennguyen.ohmygahhhh;


import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

class ChatAdaptor extends BaseAdapter {
    String name;
    Context context;
    ArrayList<ChatData> data;
    private static LayoutInflater inflater = null;

    public ChatAdaptor(Context context) {
        // TODO Auto-generated constructor stub
        this.context = context;
        this.data = new ArrayList<>();
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

//    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return data.size();
    }

//    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return data.get(position);
    }

//    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        if (data.get(position).name.equals(name)) {
            vi = inflater.inflate(R.layout.row_me, null);
        } else {
            vi = inflater.inflate(R.layout.row, null);
        }
        TextView name = (TextView) vi.findViewById(R.id.name);
        name.setText(data.get(position).name);
        TextView msg = (TextView) vi.findViewById(R.id.msg);
        msg.setText(data.get(position).msg);
        if (!data.get(position).timeStamp.isEmpty()) {
//            Log.i("thien", data.get(position).timeStamp);
//            DateFormat formatter = new SimpleDateFormat("HH:mm:ss", Locale.US);
//            formatter.setTimeZone(TimeZone.getTimeZone("UTC-07"));
//            String text = formatter.format(new Date(Long.parseLong(data.get(position).timeStamp)));

            TextView time = (TextView) vi.findViewById(R.id.time);
            time.setText(data.get(position).timeStamp);
        }
        return vi;
    }

}

