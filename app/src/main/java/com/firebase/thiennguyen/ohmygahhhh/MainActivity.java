package com.firebase.thiennguyen.ohmygahhhh;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Comment;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Queue;

public class MainActivity extends AppCompatActivity {


    ListView chatListView;
    ChatData[] fakeData = {new ChatData("Bot", "Chat đầu", "ad")};
    Map<String, ChatData> users = new HashMap<>();

    Button sendBtn;
    Button loadRoomBtn;
    EditText nameET;
    EditText roomET;
    EditText msgET;
    FirebaseUser user;
    MediaPlayer mp;
    Vibrator v;
    private static final int RC_SIGN_IN = 123;
    FirebaseAuth auth;
    DatabaseReference myRef;
    ChatAdaptor adapter;
    FirebaseDatabase database;
    ChildEventListener childEventListener;

    @Override
    protected void onStart() {
        super.onStart();
//        signin();
    }

    private GoogleSignInAccount getAccount(GoogleSignInAccount account) {
        return account;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mp = MediaPlayer.create(this, R.raw.quack);
        v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        chatListView = (ListView) findViewById(R.id.listView);
        sendBtn = (Button) findViewById(R.id.sendBtn);
        loadRoomBtn = (Button) findViewById(R.id.loadRoom);
        nameET = (EditText) findViewById(R.id.txview);
        roomET = (EditText) findViewById(R.id.room);
        msgET = (EditText) findViewById(R.id.msgTxtView);

        database = FirebaseDatabase.getInstance();
        myRef = database.getReference("message/all");
        roomET.setText("all");

        adapter = new ChatAdaptor(this);

        chatListView.setAdapter(adapter);
        addListenerForRoom("message");

        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapter.name = nameET.getText().toString();
                String secs = "" + System.currentTimeMillis();
                Date currentTime = Calendar.getInstance().getTime();


                myRef.push().setValue(new ChatData(nameET.getText().toString(), msgET.getText().toString(), currentTime.toString()));
                msgET.setText("");
            }
        });

        loadRoomBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                myRef = database.getReference(roomET.getText().toString());
                addListenerForRoom(roomET.getText().toString());
//                myRef = database.getReference(roomET.getText().toString());
                adapter.data.clear();
                myRef.get().addOnCompleteListener(new OnCompleteListener<DataSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DataSnapshot> task) {
                        if (!task.isSuccessful()) {
                            Log.e("firebase", "Error getting data", task.getException());
                        } else {
                            for (DataSnapshot dataSnapshot :
                                    Objects.requireNonNull(task.getResult()).getChildren()) {
                                ChatData chat = dataSnapshot.getValue(ChatData.class);
                                adapter.data.add(chat);
                                Log.d("firebase", String.valueOf(dataSnapshot.getValue()));
                            }
                            adapter.notifyDataSetChanged();
                            Log.d("firebase", String.valueOf(task.getResult().getChildrenCount()));
                        }
                    }
                });
            }
        });
    }

    void addListenerForRoom(String roomName) {
        if (childEventListener != null) {
            myRef.removeEventListener(childEventListener);
        }

        myRef = database.getReference("message/"+roomName);

        childEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {
                ChatData chat = dataSnapshot.getValue(ChatData.class);
                adapter.data.add(chat);
                adapter.notifyDataSetChanged();

                v.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
                mp.start();
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot snapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        };

        myRef.addChildEventListener(childEventListener);
    }

    void signin() {
        auth = FirebaseAuth.getInstance();
        if (auth.getCurrentUser() != null) {
            this.user = this.auth.getCurrentUser();
        } else {
            startActivityForResult(
                    AuthUI.getInstance()
                            .createSignInIntentBuilder()
                            .setAvailableProviders(Arrays.asList(
                                    new AuthUI.IdpConfig.GoogleBuilder().build()))
                            .build(),
                    RC_SIGN_IN);
        }
    }


    void updateUI(FirebaseUser user) {
        this.nameET.setText(user.getEmail());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            IdpResponse response = IdpResponse.fromResultIntent(data);

            if (resultCode == RESULT_OK) {
                this.user = FirebaseAuth.getInstance().getCurrentUser();
                this.updateUI(this.user);
                auth.updateCurrentUser(this.user);
            } else {

            }
        }
    }


}