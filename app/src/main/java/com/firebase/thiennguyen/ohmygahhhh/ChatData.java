package com.firebase.thiennguyen.ohmygahhhh;

public class ChatData {
    public String name;
    public String msg;
    public String timeStamp;
    public ChatData(String name, String msg, String timeStamp) {
        this.name = name;
        this.msg = msg;
        this.timeStamp = timeStamp;
    }
    public ChatData() {

    }
}
